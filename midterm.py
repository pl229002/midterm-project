#Importing tkinter functionality and HTTP requests module
from tkinter import *
import requests


#Creation of root window
root = Tk()
root.title('Robot Control GUI')
root.configure(bg='#212EFE')
root.geometry("730x385")


#Font sets
buttonFont = ("Arial", 20, "bold")
labelFont = ("Arial", 12, "bold")
inputFont = ("Arial", 14, "bold")


#The URL of the server the robot connects to, and the server's corresponding URL's
SERVER_URL = 'http://192.168.1.72:5000/%s'
FORWARD_URL = SERVER_URL % 'forward/%d'
REVERSE_URL = SERVER_URL % 'reverse/%d'
LEFT_URL = SERVER_URL % 'left/%d'
RIGHT_URL = SERVER_URL % 'right/%d'
url = ''

#GUI functions:

#Moves robot forwards
def forward():
    forwardNum = int(forwardInput.get())
    if forwardNum <= 0:
        print('Improper input. Please try again.')
    else:
        if forwardNum == 1:
            print('Moving forward 1 inch.')
        else:
            print('Moving forward ' + str(forwardNum) + " inches.")
        url = FORWARD_URL % forwardNum
        try:
            response = requests.get(url)
            if response.status_code != 200:
                print("Something went wrong " + url)
        except Exception as e:
            print("Error connecting to server")
        url = "http://192.168.1.72:5000/stop"
        try:
            response = requests.get(url)
            if response.status_code != 200:
                print("Something went wrong " + url)
        except Exception as e:
            print("Error connecting to server")

          
#Moves robot backwards
def backward():
    backwardNum = int(backwardInput.get())
    if backwardNum <= 0:
        print('Improper input. Please try again.')
    else:
        if backwardNum == 1:
            print('Moving backward 1 inch.')
        else:
            print('Moving backward ' + str(backwardNum) + " inches.")
        url = REVERSE_URL % backwardNum
        try:
            response = requests.get(url)
            if response.status_code != 200:
                print("Something went wrong " + url)
        except Exception as e:
            print("Error connecting to server")
        url = "http://192.168.1.72:5000/stop"
        try:
            response = requests.get(url)
            if response.status_code != 200:
                print("Something went wrong " + url)
        except Exception as e:
            print("Error connecting to server")


#Turns robot 90 degrees to the right
def right():
    rightNum = int(rightInput.get())
    if rightNum <= 0:
        print('Improper input. Please try again.')
    else:
        if rightNum == 1:
            print('Turning right once.')
        else:
            print('Turning right ' + str(rightNum) + " times.")
        url = RIGHT_URL % rightNum
        try:
            response = requests.get(url)
            if response.status_code != 200:
                print("Something went wrong " + url)
        except Exception as e:
            print("Error connecting to server")
        url = "http://192.168.1.72:5000/stop"
        try:
            response = requests.get(url)
            if response.status_code != 200:
                print("Something went wrong " + url)
        except Exception as e:
            print("Error connecting to server")


#Turns robot 90 degrees to the left
def left():
    leftNum = int(leftInput.get())
    if leftNum <= 0:
        print('Improper input. Please try again.')
    else:
        if leftNum == 1:
            print('Turning left once.')
        else:
            print('Turning left ' + str(leftNum) + " times.")
        url = LEFT_URL % leftNum
        try:
            response = requests.get(url)
            if response.status_code != 200:
                print("Something went wrong " + url)
        except Exception as e:
            print("Error connecting to server")
        url = "http://192.168.1.72:5000/stop"
        try:
            response = requests.get(url)
            if response.status_code != 200:
                print("Something went wrong " + url)
        except Exception as e:
            print("Error connecting to server")


#Moves robot forward 1 foot, turns right 90 degrees, moves forward another foot, then turns left 90 degrees
def special():
    print('Executing Special command.')
    url = FORWARD_URL % 12
    try:
        response = requests.get(url)
        if response.status_code != 200:
            print("Something went wrong " + url)
    except Exception as e:
        print("Error connecting to server")
    url = "http://192.168.1.72:5000/stop"
    try:
        response = requests.get(url)
        if response.status_code != 200:
            print("Something went wrong " + url)
    except Exception as e:
        print("Error connecting to server")
    url = RIGHT_URL % 1
    try:
        response = requests.get(url)
        if response.status_code != 200:
            print("Something went wrong " + url)
    except Exception as e:
        print("Error connecting to server")
    url = "http://192.168.1.72:5000/stop"
    try:
        response = requests.get(url)
        if response.status_code != 200:
            print("Something went wrong " + url)
    except Exception as e:
        print("Error connecting to server")
    url = FORWARD_URL % 12
    try:
        response = requests.get(url)
        if response.status_code != 200:
            print("Something went wrong " + url)
    except Exception as e:
        print("Error connecting to server")
    url = "http://192.168.1.72:5000/stop"
    try:
        response = requests.get(url)
        if response.status_code != 200:
            print("Something went wrong " + url)
    except Exception as e:
        print("Error connecting to server")
    url = LEFT_URL % 1
    try:
        response = requests.get(url)
        if response.status_code != 200:
            print("Something went wrong " + url)
    except Exception as e:
        print("Error connecting to server")
    url = "http://192.168.1.72:5000/stop"
    try:
        response = requests.get(url)
        if response.status_code != 200:
            print("Something went wrong " + url)
    except Exception as e:
        print("Error connecting to server")


#Ends program
def end():
    print('Ending program.')
    root.destroy()


#GUI buttons:

#Button to go forward
forwardButton = Button(root, text='Move Forward', command=forward, bg="#3BC7ED", fg="white", padx=32, pady=50)
forwardButton.grid(row=0, column=0)
forwardButton.configure(font=buttonFont)


#Button to go backward
backwardButton = Button(root, text='Move Backward', command=backward, bg="#3BC7ED", fg="white", padx=25, pady=50)
backwardButton.grid(row=0, column=1)
backwardButton.configure(font=buttonFont)


#Button to turn right
rightButton = Button(root, text='Turn Right', command=right, bg="#3BC7ED", fg="white", padx=50, pady=50)
rightButton.grid(row=0, column=2)
rightButton.configure(font=buttonFont)


#Button to turn left
leftButton = Button(root, text='Turn Left', command=left, bg="#3BC7ED", fg="white", padx=55, pady=50)
leftButton.grid(row=3, column=0)
leftButton.configure(font=buttonFont)


#Button to move forward 1 foot, turn right 90 degrees, move forward another foot, then turn left 90 degrees
specialButton = Button(root, text='Special', command=special, bg="#3BC7ED", fg="white", padx=65, pady=50)
specialButton.grid(row=3, column=1)
specialButton.configure(font=buttonFont)


#Button to quit the GUI
endButton = Button(root, text='End Program', command=end, bg="#3BC7ED", fg="white", padx=38, pady=50)
endButton.grid(row=3, column=2)
endButton.configure(font=buttonFont)


# GUI text inputs:

#Text entry box for forward function
def temporary_text_forward(event):
    if forwardInput.get() == "Enter Forward Value...":
       forwardInput.delete(0, "end")
       forwardInput.insert(0, '')
       forwardInput.config(fg = 'white')

def exit_text_forward(event):
    if forwardInput.get() == '':
        forwardInput.insert(0, "Enter Forward Value...")
        forwardInput.config(fg = 'white')
      
forwardInput = Entry(root, bg="#4380E0", fg="white")
forwardInput.grid(row=2, column=0)
forwardInput.configure(font=inputFont)
forwardInput.insert(0, "Enter Forward Value...")
forwardInput.bind('<FocusIn>', temporary_text_forward)
forwardInput.bind('<FocusOut>', exit_text_forward)


#Text entry box for backward function
def temporary_text_backward(event):
    if backwardInput.get() == "Enter Backward Value...":
       backwardInput.delete(0, "end")
       backwardInput.insert(0, '')
       backwardInput.config(fg = 'white')

def exit_text_backward(event):
    if backwardInput.get() == '':
        backwardInput.insert(0, "Enter Backward Value...")
        backwardInput.config(fg = 'white')

backwardInput = Entry(root, bg="#4380E0", fg="white")
backwardInput.grid(row=2, column=1)
backwardInput.configure(font=inputFont)
backwardInput.insert(0, "Enter Backward Value...")
backwardInput.bind('<FocusIn>', temporary_text_backward)
backwardInput.bind('<FocusOut>', exit_text_backward)


#Text entry box for right turn function
def temporary_text_right(event):
    if rightInput.get() == "Enter Right Value...":
       rightInput.delete(0, "end")
       rightInput.insert(0, '')
       rightInput.config(fg = 'white')

def exit_text_right(event):
    if rightInput.get() == '':
        rightInput.insert(0, "Enter Right Value...")
        rightInput.config(fg = 'white')

rightInput = Entry(root, bg="#4380E0", fg="white")
rightInput.grid(row=2, column=2)
rightInput.configure(font=inputFont)
rightInput.insert(0, "Enter Right Value...")
rightInput.bind('<FocusIn>', temporary_text_right)
rightInput.bind('<FocusOut>', exit_text_right)


#Text entry box for left turn function
def temporary_text_left(event):
    if leftInput.get() == "Enter Left Value...":
       leftInput.delete(0, "end")
       leftInput.insert(0, '')
       leftInput.config(fg = 'white')

def exit_text_left(event):
    if leftInput.get() == '':
        leftInput.insert(0, "Enter Left Value...")
        leftInput.config(fg = 'white')

leftInput = Entry(root, bg="#4380E0", fg="white")
leftInput.grid(row=5, column=0)
leftInput.configure(font=inputFont)
leftInput.insert(0, "Enter Left Value...")
leftInput.bind('<FocusIn>', temporary_text_left)
leftInput.bind('<FocusOut>', exit_text_left)


#Necessary for the GUI to stay open until the user closes it
root.mainloop()
